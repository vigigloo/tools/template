#!/bin/bash

if [ -z "$1" ]
then
    echo "Please provide a name as argument."
else
    find terraform_module -name '*appname*' | sed "p;s/appname/$1/" | xargs -d '\n' -n 2 mv
    sed -i "s/appname/$1/g" terraform_module/* .gitlab-ci.yml

    git rm --cached customize_template.sh
    git add terraform_module .gitlab-ci.yml
    git commit -m "feat: run renaming script"
fi
